const density = 'Ñ@#W$9876543210?!abc;:+=-,._                        '

let video
let asciiDiv

function setup() {
  noCanvas()
  video = createCapture(VIDEO)
  video.size(100, 68)
  asciiDiv = createDiv()
}

function draw() {
  video.loadPixels()

  let asciiImage = ''

  for (let j = 0; j < video.height; j++) {
    for (let i = 0; i < video.width; i++) {
      // every pixel is stored in rgbi (red,green,blue,index)(red,green,blue,index)...
      const pixelIndex = (i + j * video.width) * 4
      const r = video.pixels[pixelIndex + 0]
      const g = video.pixels[pixelIndex + 1]
      const b = video.pixels[pixelIndex + 2]
      // average brightness
      const average = (r + g + b) / 3 
      const len = density.length
      const characterIndex = floor(map(average, 0, 255, len, 0))
      const c = density.charAt(characterIndex)
      if ( c == ' ') asciiImage += '&nbsp;'
      else asciiImage += c
    }
    asciiImage += '<br/>'
  }
  asciiDiv.html(asciiImage)
}

// https://www.youtube.com/watch?v=55iwMYv8tGI
